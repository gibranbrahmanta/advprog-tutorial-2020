package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public String countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            rawAge*=2000;
        } else if (rawAge <50) {
            rawAge*=2250;
        } else {
            rawAge*=5000;
        }

        return classify(rawAge);
    }

    private String classify(int rawAge){
        if(rawAge > 100000){
            return "A class";
        }
        else if(rawAge < 20000){
            return "C class";
        }
        else{
            return "B class";
        }
    }


    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
}
