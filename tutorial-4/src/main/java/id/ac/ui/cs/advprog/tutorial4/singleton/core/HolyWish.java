package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private static HolyWish instance;

    private String wish;

    private HolyWish() {
        this.wish = "";
    }

    public static HolyWish getInstance() {
        if (instance == null ) {
            instance = new HolyWish();
        }
        return instance;
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
