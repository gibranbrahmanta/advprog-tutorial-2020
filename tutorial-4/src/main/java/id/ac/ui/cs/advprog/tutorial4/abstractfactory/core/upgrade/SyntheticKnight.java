package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class SyntheticKnight extends Knight {

    public SyntheticKnight(Armory armory) {
        this.armory = armory;
    }

    @Override
    public void prepare() {
        this.skill = this.armory.learnSkill();
        this.weapon = this.armory.craftWeapon();
    }

    @Override
    public String getDescription() {
        return "Specialized in Skill and Weapon";
    }
}
