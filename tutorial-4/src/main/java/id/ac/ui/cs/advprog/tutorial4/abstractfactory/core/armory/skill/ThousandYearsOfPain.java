package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill;

public class ThousandYearsOfPain implements Skill {

    @Override
    public String getName() {
        return "Thousand Years Of Pain";
    }

    @Override
    public String getDescription() {
        return "Longlast hit";
    }

    @Override
    public String toString() {
        return getName() + " " + getDescription();
    }
}
