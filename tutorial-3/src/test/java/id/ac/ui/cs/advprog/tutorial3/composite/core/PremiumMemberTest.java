package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals("Aqua",member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Goddess",member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        Member childMember = new OrdinaryMember("Dummy","BukanMaster");
        member.addChildMember(childMember);
        assertEquals(1,member.getChildMembers().size());

    }

    @Test
    public void testMethodRemoveChildMember() {
        Member childMember = new OrdinaryMember("Dummy","BukanMaster");
        member.addChildMember(childMember);
        member.removeChildMember(childMember);
        assertEquals(0,member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        member.addChildMember(new OrdinaryMember("a","Dummy"));
        member.addChildMember(new OrdinaryMember("b","Dummy"));
        member.addChildMember(new OrdinaryMember("c","Dummy"));
        member.addChildMember(new OrdinaryMember("d","Dummy"));
        assertEquals(3,member.getChildMembers().size());

    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member master = new PremiumMember("Test","Master");
        master.addChildMember(new PremiumMember("a","Dummy"));
        master.addChildMember(new PremiumMember("b","Dummy"));
        master.addChildMember(new PremiumMember("c","Dummy"));
        master.addChildMember(new PremiumMember("d","Dummy"));
        assertEquals(4,master.getChildMembers().size());

    }
}
