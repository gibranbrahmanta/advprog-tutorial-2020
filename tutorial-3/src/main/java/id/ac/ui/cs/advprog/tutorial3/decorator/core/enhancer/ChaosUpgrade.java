package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;

    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        Random random = new Random();
        int enhance = random.nextInt(6)+50;
        return (weapon != null ? weapon.getWeaponValue() : 0)+enhance;
    }

    @Override
    public String getDescription() {
       return this.weapon.getDescription() +" enhance with chaos upgrade";
    }
}
