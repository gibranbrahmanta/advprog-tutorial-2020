package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {

    List<Member> lstMember = new ArrayList<Member>();
    String name;
    String role;

    public PremiumMember(String name,String role) {
        this.name = name;
        this.role = role;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        if(this.role.equals("Master") || this.lstMember.size()<3 ) {
            this.lstMember.add(member);
        }
    }

    @Override
    public void removeChildMember(Member member) {
        if(lstMember.contains(member)){
            lstMember.remove(member);
        }
        else{
            return;
        }
    }

    public List<Member> getChildMembers() {
        return this.lstMember;
    }
}
