package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSummonSpell extends FamiliarSpell {

    public FamiliarSummonSpell(Familiar fam){
        super(fam);
    }
    @Override
    public String spellName() {
        return this.familiar.getRace() + ":Summon";
    }

    @Override
    public void cast() {
        this.familiar.summon();
    }
}
