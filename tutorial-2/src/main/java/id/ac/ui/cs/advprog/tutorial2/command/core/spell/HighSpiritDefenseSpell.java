package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritDefenseSpell extends HighSpiritSpell {

    public HighSpiritDefenseSpell(HighSpirit spirit){
        super(spirit);
    }

    @Override
    public String spellName() {
        return this.spirit.getRace() + ":Defense";
    }

    public void cast() {
        this.spirit.defenseStance();
    }
}
