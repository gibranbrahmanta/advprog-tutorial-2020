package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSealSpell extends FamiliarSpell {

    public FamiliarSealSpell(Familiar fam){
        super(fam);
    }

    @Override
    public String spellName() {
        return this.familiar.getRace() + ":Seal";
    }

    @Override
    public void cast() {
        this.familiar.seal();
    }
}
