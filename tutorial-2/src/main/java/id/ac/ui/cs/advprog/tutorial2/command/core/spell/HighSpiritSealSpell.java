package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritSealSpell extends HighSpiritSpell {

    public HighSpiritSealSpell(HighSpirit spirit){
        super(spirit);
    }

    @Override
    public String spellName() {
        return this.spirit.getRace() + ":Seal";
    }


    public void cast() {
        this.spirit.seal();
    }
}
